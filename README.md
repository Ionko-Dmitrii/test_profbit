# test profBIT

# Описание проекта

Страницы товаров и категорий, товары и категории кэшируются,
также есть команда для добавления категорий и товаров к ним и
команда для обновления цены, статуса и остатков товаров

# Команды
Для добавления категорий и товаров
```
$ python manage.py generate_product 1 1 (первый аргумени - кол. категорий, второй кол. товаров для каждой категории)
```
Для обновления - цены, статуса, остатков товаров
```
$ python manage.py update_product
```

# Как поднять локально проект?

Зависимости:

- PostgreSQL
- Python 3.8
- Django 3.2
- Redis

## Последовательность действий

```.bash
    $ python3.8 -m venv venv
    $ source venv/bin/activate/
    $ pip install -r req.txt
    $ создать файл .env (сделать копию .env-example)
    $ export $(grep -v '^#' .envs/.env|xargs)
```

Необходимо создать в PostgreSQL создать БД:

```.bash
    $ sudo -u postgres psql
    $ create database profbit encoding 'UTF-8';
    $ \q
```

После создания БД, необходимо применить миграцию, после запуск тестового
сервера:

```.bash
    $ python manage.py migrate
    $ python manage.py runserver
    $ redis-server
```

Если все успешно то переходите по ссылке ==> `http://locahost:8000`
