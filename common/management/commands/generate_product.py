import random

import requests

from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string

from common.models import Category, Product
from common.utils import my_delete_cache
from core.constants import STATUS_PRODUCT


class Command(BaseCommand):
    help = u'Создание категорий и товаров'

    def add_arguments(self, parser):
        parser.add_argument(
            'count_category', type=int, help=u'Количество создаваемых категорий'
        )
        parser.add_argument(
            'count_product', type=int, help=u'Количество создаваемых продуктов'
        )

    def handle(self, *args, **kwargs):
        category_count = kwargs['count_category']
        product_count = kwargs['count_product']
        category_list = list()
        product_list = list()
        for i in range(category_count):
            category = Category(name=f'Category_{get_random_string()}')
            category_list.append(category)
            for b in range(product_count):
                random_price = random.randint(0, 99234)
                random_remains = random.randint(0, 1000)
                status = random.randint(0, len(STATUS_PRODUCT) - 1)
                product = Product(
                    name=f'Product_{get_random_string()}',
                    category=category,
                    price=random_price,
                    status=STATUS_PRODUCT[status][0],
                    remains=random_remains
                )
                product_list.append(product)
        self.stdout.write(
            f'Создано {category_count} категорий и по {product_count} '
            f'товаров для каждой категории'
        )

        Category.objects.bulk_create(category_list)
        Product.objects.bulk_create(product_list)

        my_delete_cache('category_list')
        requests.get('http://localhost:8000/')
