import random

import requests
from django.core.management.base import BaseCommand

from common.models import Product, Category
from common.utils import my_delete_cache

from core.constants import STATUS_PRODUCT


class Command(BaseCommand):
    help = 'Создание категорий и товаров'

    def handle(self, *args, **kwargs):
        product_id_list = Product.objects.values_list('id', 'category_id',)
        my_delete_cache('category_pk')

        for product, category_id in product_id_list:
            random_price = random.randint(0, 99234)
            random_remains = random.randint(0, 1000)
            status = random.randint(0, len(STATUS_PRODUCT) - 1)

            Product.objects.filter(id=product).update(
                price=random_price,
                status=STATUS_PRODUCT[status][0],
                remains=random_remains,
            )

            requests.get(f'http://localhost:8000/common/product/{product}')

        for category in Category.objects.values_list('id'):
            requests.get(f'http://localhost:8000/common/category/{category}')

        self.stdout.write(
            'Поля - (цена, статус, остатки), для всех товаров обновлены!'
        )
