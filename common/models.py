import requests
from django.core.cache import cache
from django.db import models
from django.urls import reverse

from core.constants import STATUS_PRODUCT


class Category(models.Model):
    """Model for category"""

    name = models.CharField(
        verbose_name='Категория', max_length=255, unique=True
    )

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category', kwargs={'pk': self.id})


class Product(models.Model):
    """Model for product"""

    name = models.CharField(
        verbose_name='Название товара', unique=True, max_length=255
    )
    category = models.ForeignKey(
        Category, verbose_name='Категория', on_delete=models.CASCADE,
        related_name='products'
    )
    price = models.DecimalField(
        verbose_name='Цена', max_digits=10, decimal_places=2
    )
    status = models.CharField(
        verbose_name="Статус", choices=STATUS_PRODUCT, max_length=255,
        default='Out of stock'
    )
    remains = models.IntegerField(verbose_name='Остатки', default=0)

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('product', kwargs={'pk': self.id})
