from django.urls import path
from django.views.decorators.cache import cache_page

from common.views import CategoryDetailView, ProductDetailView
from core.settings import CACHES

time_cache = CACHES['default']['TIMEOUT']

urlpatterns = [
    path('category/<int:pk>', cache_page(time_cache, key_prefix='category_pk')(CategoryDetailView.as_view()), name='category'),
    path('product/<int:pk>', cache_page(time_cache, key_prefix='product')(ProductDetailView.as_view()), name='product'),
]
