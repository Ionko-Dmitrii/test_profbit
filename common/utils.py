from django.core.cache import cache


def my_delete_cache(prefix):
    arr_keys = cache.keys('*')

    for key in arr_keys:
        if prefix in key:
            cache.delete(key)
