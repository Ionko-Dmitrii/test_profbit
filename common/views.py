from django.views.generic import ListView, DetailView

from common.models import Product, Category


class CategoryView(ListView):
    """View for category"""

    template_name = 'category-list.html'
    context_object_name = 'category_list'
    queryset = Category.objects.all()
    paginate_by = 50


class CategoryDetailView(ListView):
    """View for category detail"""

    context_object_name = 'product_list'
    template_name = 'category.html'
    paginate_by = 50

    def get_queryset(self):
        queryset = Product.objects.filter(category_id=self.kwargs.get('pk'))

        return queryset

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.get(id=self.kwargs.get('pk'))

        return context


class ProductDetailView(DetailView):
    """View for product detail"""

    model = Product
    context_object_name = 'product'
    template_name = 'product.html'

    def get_queryset(self):
        queryset = Product.objects.filter(
            id=self.kwargs.get('pk')
        ).select_related('category')

        return queryset
